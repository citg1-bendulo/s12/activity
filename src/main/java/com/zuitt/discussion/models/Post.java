package com.zuitt.discussion.models;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;

//Marks this java object as a representation of an entity/record from database table "posts"
@Entity
@Table(name="posts")
public class Post {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    public Post(){}
    public Post(String title,String content){
        this.title=title;
        this.content=content;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
